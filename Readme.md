# AWX Setup

This playbook provisions a fully functional AWX system on a CentOS 7 VM. This includes:
 - A PostgresSQL database
 - AWX running through docker-compose.
 - An nginx reverse proxy, that exposes AWX via https.
 - Certbot to retrieve a valid certificate from letsencrypt.

## Configuration
The domain name and admin for the letsencrypt certificate are defined in group_vars/all.yaml.

Large filesystems (docker volumes, postgresql data, awx projects) are stored on a /data filesystem. Allocate a large enough block device to your VM and set the device path in group_vars/all.yaml.

## Credentials
Passwords are automatically created and stored in the *credentials* sub-directory. These files are not encrypted.
