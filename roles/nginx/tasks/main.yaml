---
# file: roles/nginx/tasks/main.yaml

- name: Add nginx repository
  yum_repository:
    name: nginx-stable
    description: nginx repository
    baseurl: "{{ nginx_repository }}"
    file: external_repos
    gpgcheck: yes
    gpgkey: https://nginx.org/keys/nginx_signing.key

- name: Install nginx
  yum: name=nginx state=present

- name: Enable nginx daemon
  service: name=nginx state=started enabled=yes
  ignore_errors: yes

- name: Install certbot
  get_url: url={{ certbot_url }} dest=/usr/local/bin/certbot-auto mode=0755

- name: Request certificate
  command: /usr/local/bin/certbot-auto certonly --nginx --domains {{ awx.domain }} -m {{ awx.admin_email }} -n --agree-tos
  args:
    creates: /etc/letsencrypt/live/{{ awx.domain }}/fullchain.pem

- name: Schedule automatic certificate renewal
  cron:
    name: Renew certificate
    job: 'python3 -c import random; import time; time.sleep(random.random() * 3600) && /usr/local/bin/certbot-auto renew --post-hook systemctl reload nginx -q'
    hour: '4,16'
    minute: '0'

- name: Install main nginx configuration
  template: src=nginx.conf.j2 dest=/etc/nginx/nginx.conf
  notify: Restart nginx

- name: Install vhost for AWX
  template: src=awx.conf.j2 dest=/etc/nginx/conf.d/awx.conf
  notify: Restart nginx

- name: Create snippets dir
  file: path=/etc/nginx/snippets state=directory

- name: Install snippets
  template: src={{ item }}.conf.j2 dest=/etc/nginx/snippets/{{ item }}.conf
  with_items:
    - ssl-strict
    - security-features
  notify: Restart nginx

- name: Set httpd_can_network_connect flag 
  seboolean:
    name: httpd_can_network_connect
    state: yes
    persistent: yes
